package servlets;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int wkkr = Integer.parseInt(request.getParameter("wkkr"));
		int ileRat = Integer.parseInt(request.getParameter("ileRat"));
		int oprocentowanie = Integer.parseInt(request.getParameter("oprocentowanie"));
		int oplataSt = Integer.parseInt(request.getParameter("oplataSt"));
		String rodzajRat = request.getParameter("rodzajRat");
		
		if ((request.getParameter("oblicz") != null)) {
			if (  (Integer.parseInt(request.getParameter("ileRat")) > 0) && (Integer.parseInt(request.getParameter("oprocentowanie"))>0) && ((Integer.parseInt(request.getParameter("oplataSt"))>=0)))
			{
				
		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		builder.append("<head>");
		builder.append("<style> th, td {text-align: center; border: 1px solid black;}</style>");
		builder.append("</head>");
		builder.append("<body>");	
		builder.append("<h1>Harmonogram splat</h1>");
		builder.append(createTable(wkkr, ileRat, oprocentowanie, oplataSt, rodzajRat));
		builder.append("</body>");
		builder.append("</html>");	
			
		response.getWriter().print(builder);
		}
			else {
				
				response.sendRedirect("/");
			}
				
		}
	}

	
	public StringBuilder createTable
	(int wkkr, int ileRat, double oprocentowanie, int oplataSt, String rodzajRat){
		StringBuilder builder = new StringBuilder();
		
		builder.append("<table class='Tabela' style='border: 2px solid brown;'>");
		builder.append("<tr>");
		builder.append("<th>Nr raty</th>");
		builder.append("<th>Kwota Kapitalu</th>");
		builder.append("<th>Kwota odsetek</th>");
		builder.append("<th>Oplata stala</th>");
		builder.append("<th>Calkowita kwota raty</th>");
		builder.append("</tr>");
		double calKre= wkkr;
		
		int nrRaty = 1;
		double rata = wkkr/ileRat +((oprocentowanie/100)/12);
				double stala_rata=ileRat;
				double sta=((((calKre*oprocentowanie/100)+calKre))/stala_rata);
		while (nrRaty <= ileRat){
			
			double odsetki = wkkr * oprocentowanie/100;
		    builder.append("<tr>"); builder.append("<td>");
		    builder.append(nrRaty);
		    builder.append("</td>");   builder.append("<td> ");
		    builder.append(wkkr);
		     builder.append("</td>");  builder.append("<td>");
		    builder.append(odsetki);
		    builder.append("</td>");  builder.append("<td>");
		    builder.append(oplataSt);
		    builder.append("</td>"); builder.append("<td>");
		    if (rodzajRat.contains("1")){
		    	double ro= rata + odsetki;
		    	ro *= 100;
				ro = Math.round(ro);
				ro /=100;
		    	builder.append(ro);
		    }
		    else {  	
		    	builder.append(sta);
		    	
		    }
		    builder.append("</td>");
		    builder.append("</tr>");
		    nrRaty += 1;
			wkkr -= rata;
		}
		builder.append("</table>");
			
		return builder;
	}
	
	}
	

